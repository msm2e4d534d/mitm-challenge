#!/usr/bin/env python3
import json
import socket
import struct
import sys
from utils import get_msg, send_msg, g, p, secure_rand, encrypt, decrypt


def main():
    PORT = int(sys.argv[1]) if len(sys.argv) > 1 else 13372

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(('127.0.0.1', PORT))

    # Diffie-Hellman key exchange
    a = secure_rand(2, p-2)
    g_a = pow(g, a, p)

    send_msg(sock, {'a': g_a})
    g_b = get_msg(sock)['b']

    g_ab = pow(g_b, a, p)
    shared_secret = g_ab
    print(f'Shared secret: {shared_secret}')

    send_msg(sock, encrypt(shared_secret, 'hello!'))

    # Use the shared secret as a key for symmetric encryption and authentication
    message = decrypt(shared_secret, get_msg(sock))
    print(f'Decrypted message: {message}')


if __name__ == '__main__':
    main()
