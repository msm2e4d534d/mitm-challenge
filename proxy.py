#!/usr/bin/env python3
import json
import os
import select
import socket
import struct
from utils import get_msg, send_msg, g, p, secure_rand, encrypt, decrypt


def handle_client(client_sock, addr):
    SERVER_PORT = 13371
       
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.connect(('127.0.0.1', SERVER_PORT))

    while True:
        ready_sockets, _, _ = select.select([server_sock, client_sock], [], [])
        if not ready_sockets:
            continue
        src_sock = ready_sockets[0]
        dst_sock = server_sock if src_sock == client_sock else client_sock
        try:
            msg = get_msg(src_sock)
        except:
            break  # Client disconnected
        print(msg)
        send_msg(dst_sock, msg)


def server(server_socket):
    while True:
        client_socket, addr = server_socket.accept()
        try:
            print(f'Client connected {addr[0]}:{addr[1]}')
            handle_client(client_socket, addr)
        except Exception as e:
            print(f'Client error: {e!r}')
        finally:
            print(f'Client disconnected {addr[0]}:{addr[1]}')
            client_socket.close()


def main():
    PORT = 13372
         
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('0.0.0.0', PORT))
    server_socket.listen(10)
    print(f'Server started on port {PORT}')

    try:
        server(server_socket)
    except:
        server_socket.close()
        raise


if __name__ == '__main__':
    main()
