#!/usr/bin/env python3
import socket
import json
import struct
import os
import select
from utils import get_msg, send_msg, g, p, secure_rand, encrypt, decrypt


def handle_client(client_sock, addr):
    SERVER_PORT = 13371
         
    server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_sock.connect(("127.0.0.1", SERVER_PORT))

    msg = get_msg(client_sock)
    g_a = msg["a"]
    print("g^a", g_a)

    A = secure_rand(2, p-1)
    g_A = pow(g, A, p)
    send_msg(server_sock, {"a": g_A})

    msg = get_msg(server_sock)
    g_b = msg["b"]
    print("g^b", g_b)

    g_Ab = pow(g_b, A, p)
    print("Server shared secret", g_Ab)

    B = secure_rand(2, p-1)
    g_B = pow(g, B, p)
    send_msg(client_sock, {"b": g_B})
    g_aB = pow(g_a, B, p)
    print("Client shared secret", g_aB)

    msg = get_msg(client_sock)
    plain_msg = decrypt(g_aB, msg)
    print("msg", plain_msg)
    send_msg(server_sock, encrypt(g_Ab, plain_msg))

    msg = get_msg(server_sock)
    plain_msg = decrypt(g_Ab, msg)
    print("msg", plain_msg)
    send_msg(client_sock, encrypt(g_aB, plain_msg))


def server(server_socket):
    while True:
        client_socket, addr = server_socket.accept()
        try:
            print(f"Client connected {addr[0]}:{addr[1]}")
            handle_client(client_socket, addr)
        except Exception as e:
            print(f"Client error: {e!r}")
        finally:
            print(f"Client disconnected {addr[0]}:{addr[1]}")
            client_socket.close()


def main():
    PORT = 13372
         
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", PORT))
    server_socket.listen(10)
    print(f"Server started on port {PORT}")

    try:
        server(server_socket)
    except:
        server_socket.close()
        raise


if __name__ == "__main__":
    main()
