#!/usr/bin/env python3
import json
import os
import socket
import struct
from utils import get_msg, send_msg, g, p, secure_rand, encrypt, decrypt


def handle_client(sock, addr):
    # Diffie-Hellman key exchange
    b = secure_rand(2, p-2)
    g_a = get_msg(sock)['a']

    g_b = pow(g, b, p)
    send_msg(sock, {'b': g_b})

    g_ab = pow(g_a, b, p)
    shared_secret = g_ab
    print(f'Shared secret: {shared_secret}')

    # Use the shared secret as a key for symmetric encryption and authentication
    message = decrypt(shared_secret, get_msg(sock))
    print(f'Decrypted message: {message}')

    send_msg(sock, encrypt(shared_secret, 'world!'))


def server(server_socket):
    while True:
        client_socket, addr = server_socket.accept()
        try:
            print(f'Client connected {addr[0]}:{addr[1]}')
            handle_client(client_socket, addr)
        except Exception as e:
            print(f'Client error: {e!r}')
        finally:
            print(f'Client disconnected {addr[0]}:{addr[1]}')
            client_socket.close()


def main():
    PORT = 13371
         
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('0.0.0.0', PORT))
    server_socket.listen(10)
    print(f'Server started on port {PORT}')

    try:
        server(server_socket)
    except:
        server_socket.close()
        raise


if __name__ == '__main__':
    main()
