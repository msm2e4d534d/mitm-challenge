import json
import os
import socket
import struct
from Crypto.Cipher import AES
from random import SystemRandom


# Communication utils


def u32(dword):
    '''Parse 4 bytes to an integer (big endian)'''
    return struct.unpack('>I', dword)[0]


def pack_u32(integer):
    '''Convert an integer to bytes (big endian)'''
    return struct.pack('>I', integer)


def get_msg(socket):
    d = socket.recv(4)
    assert len(d) > 0
    msg_len = u32(d)
    msg = socket.recv(msg_len).decode()
    # print(f'<<< {msg}')
    return json.loads(msg)


def send_msg(socket, message):
    # print(f'>>> {message}')
    raw_msg = json.dumps(message).encode()
    msg_len = pack_u32(len(raw_msg))
    socket.send(msg_len + raw_msg)


# Crypto constants and utils

p = 272801128439804267844248977383264236321
g = 5


def int_to_key(val):
    '''Convert an integer value to a 16-byte key'''
    return val.to_bytes(length=16, byteorder='big')


def secure_rand(from_, to):
    return SystemRandom().randint(from_, to)


def encrypt(shared_secret, text):
    key = int_to_key(shared_secret)
    cipher = AES.new(key, AES.MODE_GCM)
    ciphertext, tag = cipher.encrypt_and_digest(text.encode())
    return {
        'nonce': cipher.nonce.hex(),
        'ciphertext': ciphertext.hex(),
        'tag': tag.hex(),
    }


def decrypt(shared_secret, message):
    key = int_to_key(shared_secret)
    nonce = bytes.fromhex(message['nonce'])
    ciphertext = bytes.fromhex(message['ciphertext'])
    tag = bytes.fromhex(message['tag'])

    cipher = AES.new(key, AES.MODE_GCM, nonce=nonce)
    return cipher.decrypt_and_verify(ciphertext, tag).decode()
