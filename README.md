# MitM challenge

Celem zadania jest zaimplementowanie ataku 'man in the middle', czyli złośliwa modyfikacja pakietów przesyłanych pomiędzy dwoma stronami (skrypt `proxy.py`).

### Instalacja

Zadanie wymaga zainstalowania pakietu pycryptodome (np. za pomocą `pip install pycryptodome`).

Należy uruchomić po kolei trzy polecenia: (w osobnych terminalach)

```
$ python3 server.py
$ python3 proxy.py
$ python3 client.py
```

Efekt powinien być następujący:

#### Serwer:
```
╰─$ python3 server.py
Server started on port 13371
Client connected 127.0.0.1:36920
Shared secret: 178584142305621277623553092278904430130
Decrypted message: hello!
Client disconnected 127.0.0.1:36920
```

#### Proxy:
```
╰─$ python3 proxy.py
Server started on port 13372
Client connected 127.0.0.1:33356
{'a': 23953933916285344463951640546571120712}
{'b': 77128983586404932653615794359677888486}
{'nonce': '0559d4777f4907527bdd7b9363dcf32b', 'ciphertext': '0ed65d21b50c', 'tag': '87dd86c97e858d15e53c50d5dbb25b51'}
{'nonce': '9f3576bd48c7b4d8f7893b841f65299c', 'ciphertext': '8f82f579fff2', 'tag': 'fd9ffb15d08a44f119a10c8079dfe9cd'}
Client disconnected 127.0.0.1:33356
```

#### Klient:
```
╰─$ python3 client.py
Shared secret: 178584142305621277623553092278904430130
Decrypted message: world!
```

### Zadanie

Celem zadania jest modyfikacja pliku proxy.py (**i tylko tego pliku**) tak, żeby przechwycić i zdeszyfrować wiadomości między klientem a serwerem.

Przykładowe wyjście dla prawidłowego rozwiązania zadania:

```
╰─$ python3 proxy.py
Server started on port 13372
Client connected 127.0.0.1:36214
Client message: hello!
Server message: world!
Client disconnected 127.0.0.1:36214
```
